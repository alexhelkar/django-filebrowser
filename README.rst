Django FileBrowser Fork
==================

**Django FileBrowser without Grappelli dependency**.

Instalation
------------

pip install -e git+git://bitbucket.org/alexhelkar/django-filebrowser#egg=egg=django-filebrowser

Requirements
------------

FileBrowser 3.5 requires

* Django 1.4 (http://www.djangoproject.com)
* PIL (http://www.pythonware.com/products/pil/)

Documentation
-------------

http://readthedocs.org/docs/django-filebrowser/

Translation
-----------

https://www.transifex.net/projects/p/django-filebrowser/